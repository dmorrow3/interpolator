﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using InterpolationTester.Containers;

namespace InterpolationTester
{
    class HistoryForm : Form
    {
        RichTextBox textbox;
        public HistoryForm() : base()
        {
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Height = 500;

            textbox = new RichTextBox();
            textbox.Location = new System.Drawing.Point(5, 5);
            textbox.Width = this.Width - 30;
            textbox.Height = this.Height - 100;
            textbox.ReadOnly = true;

            Button btn = new Button();
            btn.Click += Btn_Click;
            btn.Width = 80; btn.Height = 30;
            btn.Location = new System.Drawing.Point(textbox.Width - btn.Width, textbox.Height + 15);
            btn.Text = "Clear";

            this.Controls.Add(textbox);
            this.Controls.Add(btn);
        }

        private void Btn_Click(object sender, EventArgs e)
        {
            textbox.Clear();
        }

        public void ShowSingleWindow()
        {
            if (Application.OpenForms.Count < 2)
                this.Show();
        }
        public void Load(List<History> historyList)
        {
            foreach (var item in historyList)
            {
                textbox.Text += item.distance + " " + item.type + "\n";
            }
        }
        public void Load(History h)
        {
            textbox.Text += h.distance + " " + h.type + "\n";
        }
    }
}
