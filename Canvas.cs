﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using InterpolationTester.Containers;

namespace InterpolationTester
{
    class Canvas
    {
        public Settings settings;
        private Panel panel;
        public Canvas(Panel panel)
        {
            this.panel = panel;
            panel.Paint += onPaint;
            settings = new Settings(10);
        }

        private void onPaint(object sender, PaintEventArgs e)
        {
            if (settings.drawPoints)
            {
                if (settings.points.Count > 0)
                {
                    e.Graphics.DrawRectangle(Pens.Red, settings.points[0].X, settings.points[0].Y, 2, 2);
                    e.Graphics.DrawRectangle(Pens.Purple, settings.points[settings.points.Count - 1].X, settings.points[settings.points.Count - 1].Y, 2, 2);
                }
                for (int i = 1; i < settings.points.Count - 1; i++)
                    e.Graphics.DrawRectangle(Pens.Black, settings.points[i].X, settings.points[i].Y, 1, 1);
            }
        }
        /// <summary>
        /// Draw an interpolation of the list of points
        /// </summary>
        public async Task<bool> Draw(List<PointF> pointList)
        {
            await drawLines(pointList);
            return settings.running;
        }
        public async Task<bool> Draw(Graph graph)
        {
            await drawGraph(graph);
            return settings.running;
        }

        private async Task drawLines(List<PointF> pointList)
        {
            var g = panel.CreateGraphics();
            int count = pointList.Count;
            if (count >= 2)
                for (var i = 1; i < count; i++)
                {
                    g.DrawLine(Pens.DarkRed, pointList[i-1], pointList[i]);
                    if (!settings.running)
                        break;
                    await Task.Delay(settings.drawSpeed);
                }
            g.Dispose();
        }
        private async Task drawGraph(Graph graph)
        {
            var g = panel.CreateGraphics();
            var nodeList = graph.getChildren();
            nodeList.Add(graph.getRoot());
            if (nodeList[0] == null)
            {
                g.Dispose();
                return;
            }
            foreach (var node in nodeList)
            {
                if (!settings.running)
                    break;
                for (var i = 0; i < node.nodeList.Count; i++)
                {
                    g.DrawLine(Pens.DarkRed, node.point, node.nodeList[i].point);
                    Node.Unlink(node, node.nodeList[i]);
                    i--;
                    await Task.Delay(settings.drawSpeed);
                }
            }
            
            g.Dispose();
        }

        public void Load(List<PointF> points)
        {
            settings.points = points;
            panel.Invalidate();
        }

        public async Task Invalidate()
        {
            panel.Invalidate();
            await Task.Delay(10);
        }
    }

    struct Settings
    {
        public List<PointF> points;
        public int drawSpeed;
        public bool drawPoints;
        public bool running;
        public Settings(int drawSpeed)
        {
            this.drawSpeed = drawSpeed;
            drawPoints = true;
            points = new List<PointF>();
            running = false;
        }
    }
}
