﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using InterpolationTester.Math;

namespace InterpolationTester.Containers
{
    public static class ConvertStructure
    {
        public static Graph PointlistToGraph(List<PointF> pointList, bool fullyConnected = false)
        {
            if (pointList.Count == 0)
                return new Graph();
            List<Node> nodeList = new List<Node>();
            nodeList.Capacity = pointList.Capacity;
            //Link them in a line
            nodeList.Add(new Node(pointList[0]));
            for (var i = 1; i < pointList.Count; i++)
            {
                nodeList.Add(new Node(pointList[i]));
                Node.Link(nodeList[i - 1], nodeList[i]);
            }
            if (fullyConnected)
            { //Connect every node, to every other node in the list of points
                for (var i = 1; i < nodeList.Count; i++)
                    Node.Unlink(nodeList[i - 1], nodeList[i]); //Unlink prior point-to-point set

                for (var i = 0; i < nodeList.Count; i++)
                {
                    for (var j = i; j < nodeList.Count; j++)
                    {
                        if (i == j)
                            continue;
                        else
                        {
                            Node.Link(nodeList[i], nodeList[j]);
                        }
                    }
                }
            }
            return new Graph(nodeList);
        }
        /// <summary>
        /// Spatially link points together by the order in the list
        /// </summary>
        /// <param name="graph"></param>
        /// <returns></returns>
        public static List<PointF> NodeListToPointList(List<Node> nodeList)
        {
            List<PointF> pointList = new List<PointF>();
            pointList.Capacity = nodeList.Count;
            foreach (Node n in nodeList)
            {
                pointList.Add(n.point);
            }
            return pointList;
        }
    }
    public class Graph
    {
        List<Node> nodeList; //children
        Node root; //parent
        public Graph()
        {
            nodeList = new List<Node>();
        }
        public Graph(List<Node> nodeList)
        {
            this.nodeList = new List<Node>();
            bool firstNode = true;
            foreach (var node in nodeList)
            {
                if (firstNode)
                {
                    firstNode = false;
                    this.root = node;
                }
                else
                    this.nodeList.Add(node);
            }

        }
        public void Add(Node n)
        {
            if (root == null)
            {
                root = n;
            }
            else
                nodeList.Add(n);
        }
        public Node getRoot()
        {
            return root;
        }
        public void setRoot(Node n)
        {
            this.root = n;
        }
        public Node getChildByIndex(int index)
        {
            if (index < nodeList.Count)
                return nodeList.ElementAt(index);
            return null;
        }
        public void setChildByIndex(int index, Node n)
        {
            nodeList[index] = n;
        }
        public List<Node> getChildren()
        {
            return nodeList;
        }

        public float getTotalDistance()
        {
            float distance = 0;
            var nodeListCopy = nodeList.ToList();
            if (nodeListCopy[0] == null)
                return distance;
            nodeListCopy.Add(new Node(root.point));
            for (var i = 0; i < nodeListCopy.Count; i++)
            {
                for (var j = i; j < nodeListCopy.Count; j++)
                {
                    if (i != j)
                    {
                        Vector v = new Vector(nodeListCopy[i].point.X, nodeListCopy[j].point.X, nodeListCopy[i].point.Y, nodeListCopy[j].point.Y);
                        distance += v.getMagnitude();
                        Node.Unlink(nodeListCopy[i], nodeListCopy[j]);
                    }
                }
            }
            return distance;
        }
    }
    public class Node
    {
        public Guid Id;
        public List<Node> nodeList;
        public PointF point;
        public Node()
        {
            Id = Guid.NewGuid();
            nodeList = new List<Node>();
            point.X = 0;
            point.Y = 0;
        }
        public Node(PointF point)
        {
            Id = Guid.NewGuid();
            nodeList = new List<Node>();
            this.point = point;
        }
        public Node(Node n)
        {
            this.Id = n.Id;
            this.nodeList = n.nodeList;
            this.point = n.point;
        }
        public static void Link(Node n1, Node n2)
        {
            n1.nodeList.Add(n2);
            n2.nodeList.Add(n1);
        }
        public static void Unlink(Node n1, Node n2)
        {
            n1.nodeList.RemoveAll(x => x.Id == n2.Id);
            n2.nodeList.RemoveAll(x => x.Id == n1.Id);
        }
        /// <summary>
        /// Create a semi-deep copy of the passed node. Reference to children are retained, children are not copied.
        /// </summary>
        /// <param name="n">The node to be copied</param>
        /// <returns></returns>
        public static Node Copy(Node n)
        {
            Node copiedNode = new Node(n.point); //Structs are 'by value' so this works.
            copiedNode.nodeList.Capacity = n.nodeList.Capacity;
            copiedNode.Id = n.Id;
            copiedNode.nodeList = n.nodeList.ToList(); //Can't deep copy children unless we check for reference loops
            return copiedNode;
        }
        /// <summary>
        /// Determine whether two nodes are in the same set (doubly referenced nodes)
        /// </summary>
        /// <param name="n1"></param>
        /// <param name="n2"></param>
        /// <returns></returns>
        public static bool isSameSet(Node n1, Node n2)
        {
            //Checking if n2 is in n1 will suffice
            List<Node> visited = new List<Node>();
            Queue<Node> queue = new Queue<Node>();
            queue.Enqueue(n1); visited.Add(n1);
            while (queue.Count > 0)
            {
                Node n = Node.Copy(queue.Dequeue());
                foreach (var node in n.nodeList)
                {
                    if (visited.FirstOrDefault(x => x.Id == node.Id) == null)
                    {
                        queue.Enqueue(node);
                        if (node.Id == n2.Id)
                            return true;
                    }
                }
            }
            return false;
        }
        public void Add(Node n)
        {
            this.nodeList.Add(n);
        }
        public Node findByPoint(PointF p)
        {
            Queue<Node> queue = new Queue<Node>();
            foreach (var node in nodeList)
            {
                if (node.point.X == p.X && node.point.Y == p.Y)
                    return node;
                else
                {
                    queue.Enqueue(node);
                }
            }
            while (queue.Count > 0)
            {
                var node = queue.Dequeue();
                var result = node.findByPoint(p);
                if (result != null)
                    return result;
            }
            return null;
        }
    }

    public class PriorityQueue<T>
    {
        QNode root;
        public PriorityQueue() { }
        public void Enqueue(T obj, double priority)
        {
            if (root != null)
            {
                root.Enqueue(obj, priority);
            }
            else
                root = new QNode(priority, obj, null);
        }
        public T Dequeue()
        {
            if (root != null)
            {
                var obj = root.RNL();
                if (root.qCount() == 0)
                {
                    if (root.Left != null)
                        root = root.Left; //Move the root down the left branch
                    else
                        root = null;
                }
                return obj;
            }
            else
                return default(T);
        }
        public int Count()
        {
            if (root == null)
                return 0;
            return root.countRecursively();
        }

        private class QNode
        {
            private double priority;
            private Queue<T> q = new Queue<T>();
            private QNode parent;
            private QNode left;
            private QNode right;
            public QNode(QNode parent) { priority = 0; this.parent = parent; }
            public QNode(double priority, T obj, QNode parent) { this.priority = priority; this.q.Enqueue(obj); this.parent = parent; }
            public double Priority { get { return priority; } }
            public QNode Parent { get { return parent; } set { parent = value; } }
            public QNode Right { get { return right; } set { right = value; } }
            public QNode Left { get { return left; } set { left = value; } }
            public T RLN() //Dequeue the top priority object
            {
                if (right != null)
                { //check if the queue is empty on the right
                    var obj = right.RLN();
                    if (right.qCount() == 0)
                        right = null;
                    return obj;
                }
                if (left != null)
                { //check if the queue is empty on the left
                    var obj = left.RLN();
                    if (left.qCount() == 0)
                        left = null;
                    return obj;
                }
                return q.Dequeue();
            }
            public T RNL()
            {
                if (right != null)
                {
                    var obj = right.RNL();
                    if (right.qCount() == 0)
                        right = null;
                    return obj;
                }
                var thisObj = q.Dequeue();
                if (q.Count == 0)
                {
                    if (parent == null) //Is this the root?
                        return thisObj;
                    else if (left != null)
                        parent.Right = left; //Tie the paren't "right" branch to this node's "left" branch
                }
                return thisObj;
                //if (left != null) //Unreachable code, we never traverse left
                //{
                //    var obj = left.RNL();
                //    if (right.qCount() == 0)
                //        right = null;
                //    return obj;
                //}
            }
            public void Enqueue(T obj, double priority)
            {
                if (this.priority == priority)
                    q.Enqueue(obj);
                else if (this.priority < priority)
                {
                    if (right != null)
                        right.Enqueue(obj, priority);
                    else
                        right = new QNode(priority, obj, this);
                }
                else if (this.priority > priority)
                {
                    if (left != null)
                        left.Enqueue(obj, priority);
                    else
                        left = new QNode(priority, obj, this);
                }
            }
            public int qCount()
            {
                return q.Count;
            }
            public int countRecursively() //Traverse like LRN
            {
                int sum = 0;
                if (left != null)
                    sum += left.countRecursively();
                if (right != null)
                    sum += right.countRecursively();
                return sum + qCount();
            }
        }
    }

    

    struct History
    {
        public float distance;
        public string type;
        public History(float distance, string type)
        {
            this.distance = distance;
            this.type = type;
        }
    }
}
