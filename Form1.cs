﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using InterpolationTester.Algorithms;
using InterpolationTester.Containers;

namespace InterpolationTester
{
    public partial class Form1 : Form
    {
        HistoryForm h = new HistoryForm();
        List<History> historyList;
        Canvas canvas;
        public Form1()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            panel.GetType().GetProperty("DoubleBuffered", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic).SetValue(panel, true, null);
        }
        private void drawPointsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!drawPointsToolStripMenuItem.Checked)
            {
                canvas.settings.drawPoints = true;
                drawPointsToolStripMenuItem.Checked = true;
                drawPointsToolStripMenuItem.BackColor = Color.Gainsboro;
            }
            else
            {
                canvas.settings.drawPoints = false;
                drawPointsToolStripMenuItem.Checked = false;
                drawPointsToolStripMenuItem.BackColor = Color.WhiteSmoke;
            }
            canvas.Invalidate();
        }
        private void paintModeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!paintModeToolStripMenuItem.Checked)
            {
                paintModeToolStripMenuItem.Checked = true;
                paintModeToolStripMenuItem.BackColor = Color.Gainsboro;
            }
            else
            {
                paintModeToolStripMenuItem.Checked = false;
                paintModeToolStripMenuItem.BackColor = Color.WhiteSmoke;
            }
        }

        private void pencilToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var open = new OpenFileDialog())
            {
                if (open.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        BinaryFormatter formatter = new BinaryFormatter();
                        using (var file = (Stream)open.OpenFile())
                        {
                            var pointList = (List<PointF>)formatter.Deserialize(file);
                            canvas.Load(pointList);
                        }
                    }
                    catch { MessageBox.Show("There was a problem reading the file."); }
                }
            }

        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var save = new SaveFileDialog())
            {
                save.Filter = "Point|*.pt";
                save.Title = "Save List of Points";
                if (save.ShowDialog() == DialogResult.OK)
                {
                    string filename = save.FileName;
                    BinaryFormatter formatter = new BinaryFormatter();
                    using (var stream = (FileStream)save.OpenFile())
                    {
                        formatter.Serialize(stream, canvas.settings.points);
                    }
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            canvas = new Canvas(this.panel);
            ddlAlgorithm.Items.AddRange(Algorithm.getImplementations().ToArray());
            historyList = new List<History>();
        }

        private void speedBar_ValueChanged(object sender, EventArgs e)
        {
            canvas.settings.drawSpeed = speedBar.Value;
        }

        private async void runBtn_Click(object sender, EventArgs e)
        {
            if (!canvas.settings.running)
            {
                await canvas.Invalidate();
                canvas.settings.running = true;
                runBtn.Text = "Stop"; 

                Task.WaitAll();
                string selectedName = ddlAlgorithm.Text;
                List<PointF> pointList = null;
                Graph graph = null;
                if (!typechkBox.Checked)
                    pointList = !string.IsNullOrEmpty(ddlAlgorithm.Text) ? await Algorithm.run(selectedName, canvas) : canvas.settings.points;
                else
                    graph = !string.IsNullOrEmpty(ddlAlgorithm.Text) ? await Algorithm.runGraph(selectedName, canvas) : InterpolationTester.Containers.ConvertStructure.PointlistToGraph(canvas.settings.points);

                if (typechkBox.Checked)
                {
                    bool completed = await canvas.Draw(graph);
                    if (completed)
                    {
                        canvas.settings.running = false;
                        float distance = graph.getTotalDistance();
                        History newhistory = new History(distance, selectedName);
                        h.Load(newhistory);
                        historyList.Add(newhistory);
                        runBtn.Text = "Run";
                    }
                }
                else
                {
                    bool completed = await canvas.Draw(pointList);
                    if (completed)
                    {
                        canvas.settings.running = false;
                        float distance = pointList.getDistance();
                        History newhistory = new History(distance, selectedName);
                        h.Load(newhistory);
                        historyList.Add(newhistory);
                        runBtn.Text = "Run";
                    }
                }
            }
            else
            {
                canvas.settings.running = false;
                runBtn.Text = "Run";
            }
        }


        private void panel_MouseDown(object sender, MouseEventArgs e)
        {
            PointF mPoint = new PointF(e.X, e.Y);
            if (paintModeToolStripMenuItem.Checked && !canvas.settings.points.Any(pt => pt.X == mPoint.X && pt.Y == mPoint.Y))
            {
                canvas.settings.points.Add(mPoint);
                canvas.Invalidate();
            }
            else
            {
                float minDistance = float.MaxValue;
                int index = 0;
                for (int i = 0; i < canvas.settings.points.Count; i++)
                {
                    float x = canvas.settings.points[i].X - mPoint.X;
                    float y = canvas.settings.points[i].Y - mPoint.Y;
                    float distance = (float)System.Math.Sqrt(x * x + y * y);
                    if (distance < minDistance)
                    {
                        minDistance = distance;
                        index = i;
                    }
                }
                if (canvas.settings.points.Count > 0)
                {
                    if (e.Button == MouseButtons.Left)
                    {
                        PointF holdPoint = canvas.settings.points[index];
                        canvas.settings.points[index] = canvas.settings.points[0];
                        canvas.settings.points[0] = holdPoint;
                    }
                    else if (e.Button == MouseButtons.Right)
                    {
                        PointF holdPoint = canvas.settings.points[index];
                        canvas.settings.points[index] = canvas.settings.points[canvas.settings.points.Count - 1];
                        canvas.settings.points[canvas.settings.points.Count - 1] = holdPoint;
                    }
                    canvas.Invalidate();
                }
            }
        }

        private void clearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            canvas.settings.points.Clear();
            canvas.Invalidate();
        }
        

        private void viewHistoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (h.IsDisposed)
                h = new HistoryForm();
            h.ShowSingleWindow();
            h.Load(historyList);
        }

        private void typechkBox_CheckedChanged(object sender, EventArgs e)
        {
            var thisChkBox = (CheckBox)sender;
            ddlAlgorithm.Items.Clear();
            if (!thisChkBox.Checked)
                ddlAlgorithm.Items.AddRange(Algorithm.getImplementations().ToArray());
            else
                ddlAlgorithm.Items.AddRange(Algorithm.getImplementations(graph: true).ToArray());
            ddlAlgorithm.Text = "";
        }
    }
}
