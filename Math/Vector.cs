﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace InterpolationTester.Math
{
    public class Vector
    {
        private float X, Y, Z;
        public float posX, posY, posZ;
        public float terminalX, terminalY, terminalZ;
        public float deltaX { get { return this.X; } }
        public float deltaY { get { return this.Y; } }
        public float deltaZ { get { return this.Z; } }
        /// <summary>
        /// Create a Vector with an assumed position at the origin
        /// </summary>
        /// <param name="chgX"></param>
        /// <param name="chgY"></param>
        /// <param name="chgZ"></param>
        public Vector(float chgX, float chgY, float chgZ = 0)
        {
            X = chgX; posX = 0;
            Y = chgY; posY = 0;
            Z = chgZ; posZ = 0;
        }
        /// <summary>
        /// Create a Vector with a defined position
        /// </summary>
        /// <param name="posX"></param>
        /// <param name="posY"></param>
        /// <param name="magX"></param>
        /// <param name="magY"></param>
        /// <param name="posZ"></param>
        /// <param name="magZ"></param>
        public Vector(float posX, float posY, float magX, float magY, float posZ = 0, float magZ = 0)
        {
            X = magX - posX;
            Y = magY - posY;
            Z = magZ - posZ;

            this.posX = posX;
            this.posY = posY;
            this.posZ = posZ;
            this.terminalX = magX;
            this.terminalY = magY;
            this.terminalZ = magZ;
        }

        public float getMagnitude()
        {
            return (float)System.Math.Sqrt(X * X + Y * Y + Z * Z);
        }
        public float getSlope2D()
        {
            return (Y / X);
        }
        public float dotProduct(Vector v)
        {
            return (this.X * v.X + this.Y * v.Y + this.Z * v.Z);
        }
        public Vector crossProduct(Vector v)
        {
            var orthogonalVector = new Vector(this.Y * v.Z - v.Y * this.Z, -(this.X * v.Z - v.X * this.Z), this.X * v.Y - v.X * this.Y);
            return orthogonalVector;
        }
        public static float getAngleBetween(Vector v1, Vector v2)
        {
            var dotProd = v1.dotProduct(v2);
            var cos = dotProd / (v1.getMagnitude() * v2.getMagnitude());
            var angle = (float)System.Math.Acos(cos);
            return angle;
        }

        /// <summary>
        /// Determine whether two vectors are crossing
        /// </summary>
        /// <param name="v">Vector with a defined position</param>
        /// <returns></returns>
        public bool isIntersecting2D(Vector v)
        {
            //http://www.geeksforgeeks.org/check-if-two-given-line-segments-intersect/
            // Find the four orientations needed for general and
            // special cases
            int o1 = orientation(new PointF(this.posX, this.posY), new PointF(this.terminalX, this.terminalY), new PointF(v.posX, v.posY));
            int o2 = orientation(new PointF(this.posX, this.posY), new PointF(this.terminalX, this.terminalY), new PointF(v.terminalX, v.terminalY));
            int o3 = orientation(new PointF(v.posX, v.posY), new PointF(v.terminalX, v.terminalY), new PointF(this.posX, this.posY));
            int o4 = orientation(new PointF(v.posX, v.posY), new PointF(v.terminalX, v.terminalY), new PointF(this.terminalX, this.terminalY));

            // General case
            if (o1 != o2 && o3 != o4)
                return true;

            // Special Cases
            // p1, q1 and p2 are colinear and p2 lies on segment p1q1
            if (o1 == 0 && onSegment(new PointF(this.posX, this.posY), new PointF(v.posX, v.posY), new PointF(this.terminalX, this.terminalY))) return true;

            // p1, q1 and p2 are colinear and q2 lies on segment p1q1
            if (o2 == 0 && onSegment(new PointF(this.posX, this.posY), new PointF(v.terminalX, v.terminalY), new PointF(this.terminalX, this.terminalY))) return true;

            // p2, q2 and p1 are colinear and p1 lies on segment p2q2
            if (o3 == 0 && onSegment(new PointF(v.posX, v.posY), new PointF(this.posX, this.posY), new PointF(v.terminalX, v.terminalY))) return true;

            // p2, q2 and q1 are colinear and q1 lies on segment p2q2
            if (o4 == 0 && onSegment(new PointF(v.posX, v.posY), new PointF(this.terminalX, this.terminalY), new PointF(v.terminalX, v.terminalY))) return true;

            return false; // Doesn't fall in any of the above cases

        }
        private int orientation(PointF p, PointF q, PointF r)
        {
            // See http://www.geeksforgeeks.org/orientation-3-ordered-points/
            // for details of below formula.
            float val = (q.Y - p.Y) * (r.X - q.X) -
                      (q.X - p.X) * (r.Y - q.Y);

            if (val == 0) return 0;  // colinear

            return (val > 0) ? 1 : 2; // clock or counterclock wise
        }
        private bool onSegment(PointF p, PointF q, PointF r)
        {
            if (q.X <= System.Math.Max(p.X, r.X) && q.X >= System.Math.Min(p.X, r.X) &&
                q.Y <= System.Math.Max(p.Y, r.Y) && q.X >= System.Math.Min(p.Y, r.Y))
                return true;
            return false;
        }
    }
}
