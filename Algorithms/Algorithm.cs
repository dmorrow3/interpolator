﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using InterpolationTester.Math;
using InterpolationTester.Algorithms;
using InterpolationTester.Containers;

namespace InterpolationTester
{
    static class Algorithm
    {
        public static List<string> getImplementations(bool graph = false)
        {
            List<string> implements = new List<string>();
            if (graph)
            {
                implements.Add("Complete Graph");
                implements.Add("Cyclic Hull");
                implements.Add("Web Graph");
            }
            else
            {
                implements.Add("Nearest Neighbor");
                implements.Add("NN Uncross");
                implements.Add("Angular Uncross");
                implements.Add("Radial Uncross");
                implements.Add("Angular Neighbor");
                implements.Add("Radial Neighbor");
                implements.Add("Convex Hull");
                implements.Add("Shortest Path");
                implements.Add("Dijkstra's Network");
            }
            return implements;
        }
        public static Task<List<PointF>> run(string selection, Canvas c)
        {
            switch (selection)
            {
                case "Nearest Neighbor":
                    {
                        return Task.Run(() => NearestNeighbor(c.settings.points));
                    }
                case "NN Uncross":
                    {
                        return Task.Run(() => NNUncrossing(c.settings.points));
                    }
                case "Angular Uncross":
                    {
                        return Task.Run(() => AngularUncrossing(c.settings.points));
                    }
                case "Radial Uncross":
                    {
                        return Task.Run(() => RadialUncrossing(c.settings.points));
                    }
                case "Angular Neighbor":
                    {
                        return Task.Run(() => AngularNeighbor(c.settings.points));
                    }
                case "Radial Neighbor":
                    {
                        return Task.Run(() => RadialNeighbor(c.settings.points));
                    }
                case "Convex Hull":
                    {
                        return Task.Run(() => ConvexHull(c.settings.points));
                    }
                case "Shortest Path":
                    {
                        return Task.Run(() => ShortestPath(ConvertStructure.PointlistToGraph(c.settings.points, true)));
                    }
                case "Dijkstra's Network":
                    {
                        //var graph = ConvertStructure.PointlistToGraph(c.settings.points, true);
                        var graph = WebGraph(c.settings.points.ToList());

                        List<Node> nodeList = new List<Node>(graph.getChildren());
                        nodeList.Add(graph.getRoot());
                        Node endNode = null;
                        foreach (var n in nodeList)
                            if (n.point.X == c.settings.points[c.settings.points.Count - 1].X && n.point.Y == c.settings.points[c.settings.points.Count - 1].Y)
                            {
                                endNode = n;
                                break;
                            }
                        int i = 0;
                        foreach (var n in nodeList)
                        {
                            if (n.point.X == c.settings.points[0].X && n.point.Y == c.settings.points[0].Y)
                            {//Swap root with the node that should be the 'actual' root (start point)
                                var oldroot = new Node(graph.getRoot());
                                graph.setRoot(n);
                                if (i - 1 == 0)
                                    graph.setChildByIndex(0, oldroot);
                                else
                                    graph.setChildByIndex(i - 1, oldroot);
                            }
                            i += 1;
                        }

                        return Task.Run(() => Dijkstras(graph, endNode));
                    }
            }
            
            return Task.Run(() => new List<PointF>());
        }
        public static Task<Graph> runGraph(string selector, Canvas c)
        {
            if (c.settings.points.Count > 0)
            switch (selector)
            {
                case "Complete Graph":
                    {
                        return Task.Run(() => CompleteGraph(ConvertStructure.PointlistToGraph(c.settings.points, true)));
                    }
                case "Cyclic Hull":
                    {
                        return Task.Run(() => CyclicHull(c.settings.points));
                    }
                case "Web Graph":
                    {
                        return Task.Run(() => WebGraph(c.settings.points));
                    }
            }
            return Task.Run(() => new Graph());
        }
        private static List<PointF> Dijkstras(Graph graph, Node endpoint)
        {
            PriorityQueue<PathContainer> queue = new PriorityQueue<PathContainer>();
            var initialPath = new List<Node>(); var initialVisited = new List<Node>();
            initialPath.Add(graph.getRoot()); initialVisited.Add(graph.getRoot());
            queue.Enqueue(new PathContainer { node = graph.getRoot(), path = initialPath, visited = initialVisited, priority = 0.0 }, 0);
            while (queue.Count() != 0)
            {
                var content = queue.Dequeue();
                if (content.node.Id == endpoint.Id)
                    return ConvertStructure.NodeListToPointList(content.path);
                else
                {
                    foreach (Node n in content.node.nodeList)
                    {
                        List<Node> visited = content.visited;
                        if (!visited.Any(x => x.Id == n.Id))
                        {
                            var newpath = new List<Node>(content.path); newpath.Add(n);
                            var newvisited = new List<Node>(content.visited); newvisited.Add(n);
                            double newpriority = (new Vector(n.point.X - content.node.point.X, n.point.Y - content.node.point.Y)).getMagnitude() + content.priority; //edgeLength + total length thus far
                            queue.Enqueue(new PathContainer { node = n, path = newpath, visited = newvisited, priority = newpriority }, 1 / newpriority); // newpriority should never be 0 (2 points occupying same location)
                        }
                    }
                }
            }
            return ConvertStructure.NodeListToPointList(initialPath);
        }
        private static List<PointF> ShortestPath(Graph graph)
        {
            var allNodes = new List<Node>(graph.getChildren()); allNodes.Add(graph.getRoot()); //List of all distinct nodes
            PriorityQueue<PathContainer> queue = new PriorityQueue<PathContainer>();
            var initialPath = new List<Node>(); var initialVisited = new List<Node>();
            initialPath.Add(graph.getRoot()); initialVisited.Add(graph.getRoot());
            queue.Enqueue(new PathContainer{ node = graph.getRoot(), path = initialPath, visited = initialVisited, priority = 0.0 }, 0);
            while (queue.Count() != 0)
            {
                var content = queue.Dequeue();
                if (content.visited.Count == allNodes.Count)
                    return ConvertStructure.NodeListToPointList(content.path);
                else
                {
                    foreach (Node n in content.node.nodeList)
                    {
                        List<Node> visited = content.visited;
                        if (!visited.Any(x => x.Id == n.Id))
                        {
                            var newpath = new List<Node>(content.path); newpath.Add(n);
                            var newvisited = new List<Node>(content.visited); newvisited.Add(n);
                            double newpriority = (new Vector(n.point.X - content.node.point.X, n.point.Y - content.node.point.Y)).getMagnitude() + content.priority; //edgeLength + total length thus far
                            queue.Enqueue(new PathContainer{ node = n, path = newpath, visited = newvisited, priority = newpriority }, 1/newpriority); // newpriority should never be 0 (2 points occupying same location)
                        }
                    }
                }
            }
            return ConvertStructure.NodeListToPointList(initialPath);
        }
        private class PathContainer
        {
            public Node node { get; set; }
            public List<Node> path { get; set; }
            public List<Node> visited { get; set; }
            public double priority { get; set; }
        }
        private static Graph WebGraph(List<PointF> pointList)
        {
            List<Node> nodeList = new List<Node>();
            var outerHull = ConvexHull(pointList, true);
            pointList = pointList.Where(p => !outerHull.Any(h => h.X == p.X && h.Y == p.Y)).ToList(); //remove outerhull points
            if (outerHull.Count > 1)
                outerHull.RemoveAt(outerHull.Count - 1); //the first & last pt are the same, drop one of them.

            List<Node> tempList = new List<Node>();
            foreach (var pt in outerHull)
            {
                tempList.Add(new Node(pt));
            }
            for (int i = 1; i < tempList.Count; i++)
            {
                Node.Link(tempList[i], tempList[i - 1]);
            }
            Node.Link(tempList[0], tempList[tempList.Count - 1]); //link the start node with the end node

            nodeList.AddRange(tempList);
            nodeList.AddRange(connectHullsRecursively(tempList, pointList));
            
            return new Graph(nodeList);
        }
        private static IEnumerable<Node> connectHullsRecursively(List<Node> outerHull, List<PointF> pointList)
        {
            List<Node> nodeList = new List<Node>();
            if (pointList.Count == 0)
                return outerHull;
            else
            {
                var innerHull = ConvexHull(pointList, true);
                {// Link hull points together & add to nodeList
                    if (innerHull.Count > 1)
                        innerHull.RemoveAt(innerHull.Count - 1); //the first & last pt are the same, drop one of them.
                    List<Node> tempList = new List<Node>();
                    foreach (var pt in innerHull)
                    {
                        tempList.Add(new Node(pt));
                    }
                    for (int i = 1; i < tempList.Count; i++)
                    {
                        Node.Link(tempList[i], tempList[i - 1]);
                    }
                    Node.Link(tempList[0], tempList[tempList.Count - 1]); //link the start node with the end node
                    var tempList2 = tempList.ToList(); //Create a copy (we're going to modify this in-loop)
                    outerHull.Add(outerHull[0]);
                    for (int i = 1; i < outerHull.Count; i++)
                    {
                        var vectorOuter = new Vector(outerHull[i].point.X - outerHull[i - 1].point.X, outerHull[i].point.Y - outerHull[i - 1].point.Y);
                        var ordered = tempList2.OrderBy(node => 0.5 * vectorOuter.getMagnitude() * //height = edgeVector.Mag() * sin(theta)
                        ((new Vector(outerHull[i-1].point.X - node.point.X, outerHull[i-1].point.Y - node.point.Y)).getMagnitude() * System.Math.Sin(Vector.getAngleBetween(vectorOuter, (new Vector(outerHull[i - 1].point.X - node.point.X, outerHull[i - 1].point.Y - node.point.Y)))))); //Order by area of a triangle
                        if (!outerHull[i-1].nodeList.Any(n => n.point.X == ordered.ElementAt(0).point.X && n.point.Y == ordered.ElementAt(0).point.Y))
                            Node.Link(outerHull[i - 1], ordered.ElementAt(0));
                        if (!outerHull[i].nodeList.Any(n => n.point.X == ordered.ElementAt(0).point.X && n.point.Y == ordered.ElementAt(0).point.Y))
                            Node.Link(outerHull[i], ordered.ElementAt(0));
                    }

                    pointList = pointList.Where(p => !innerHull.Any(h => h.X == p.X && h.Y == p.Y)).ToList(); //remove innerhull points
                    nodeList.AddRange(tempList); //add the inner nodes
                    nodeList.AddRange(connectHullsRecursively(tempList, pointList)); //recursively add inner hull nodes
                    return nodeList;
                }


            }
        }
        private static Graph CompleteGraph(Graph graph) 
        {
            return graph;
        }
        private static List<PointF> NearestNeighbor(List<PointF> pointList)
        {
            var orderedList = new List<PointF>();
            orderedList.Capacity = pointList.Capacity;
            
            for (var i = 0; i < pointList.Count-1; i++)
            {
                //await Task.Delay(1); //Keep Application Responsive
                float max = float.MaxValue;
                for (var j = i + 1; j < pointList.Count; j++)
                {
                    var vector = new Vector(System.Math.Abs(pointList[i].X - pointList[j].X), System.Math.Abs(pointList[i].Y - pointList[j].Y));
                    float distance = vector.getMagnitude();
                    if (max > distance) { 
                        max = distance;
                        PointF point = pointList[i+1];
                        pointList[i + 1] = pointList[j];
                        pointList[j] = point;
                    }
                }
            }
            orderedList.AddRange(pointList);
            return orderedList;
        }
        
        private static List<PointF> NNUncrossing(List<PointF> pointList)
        {
            pointList = NearestNeighbor(pointList);
            return Uncross(pointList);
        }
        private static List<PointF> AngularUncrossing(List<PointF> pointList)
        {
            pointList = AngularNeighbor(pointList);
            return Uncross(pointList);
        }
        private static List<PointF> RadialUncrossing(List<PointF> pointList)
        {
            pointList = RadialNeighbor(pointList);
            return Uncross(pointList);
        }

        private static List<PointF> AngularNeighbor(List<PointF> pointList)
        {
            var orderedList = new List<PointF>();
            orderedList.Capacity = pointList.Capacity;

            PointF originPoint = pointList[0];
            var vectorList = new List<Vector>();
            vectorList.Capacity = pointList.Capacity - 1;
            for (int i = 1; i < pointList.Count; i++)
            {
                PointF p = pointList[i];
                Vector v = new Vector(originPoint.X, originPoint.Y, p.X, p.Y);
                vectorList.Add(v);
            }

            orderedList.Add(originPoint);
            orderedList.AddRange(vectorList.OrderBy(v => System.Math.Atan(v.getSlope2D())).Select(v => new PointF(v.terminalX, v.terminalY)));

            return orderedList;
        }
        private static List<PointF> RadialNeighbor(List<PointF> pointList)
        {
            var orderedList = new List<PointF>();
            orderedList.Capacity = pointList.Capacity;

            PointF originPoint = pointList[0];
            var vectorList = new List<Vector>();
            vectorList.Capacity = pointList.Capacity - 1;
            for (int i = 1; i < pointList.Count; i++)
            {
                PointF p = pointList[i];
                Vector v = new Vector(originPoint.X, originPoint.Y, p.X, p.Y);
                vectorList.Add(v);
            }

            orderedList.Add(originPoint);
            orderedList.AddRange(vectorList.OrderBy(v => v.getMagnitude()).Select(v => new PointF(v.terminalX, v.terminalY)));
            
            return orderedList;
        }
        private static List<PointF> ConvexHull(List<PointF> pointList, bool extremePoint = false)
        {
            if (pointList.Count < 2)
                return pointList;

            if (extremePoint)
            {
                int index = pointList.indexOfminXPoint();
                PointF extremePt = pointList[index];
                pointList[index] = pointList[0];
                pointList[0] = extremePt;
            }

            var hullStack = new Stack<PointF>();
            pointList = AngularNeighbor(pointList);
            hullStack.Push(pointList[0]); //First point is always a hull point
            hullStack.Push(pointList[1]); //Assume (for now) the next point is a hull point
            for (int i = 2; i < pointList.Count; i++)
            {
                PointF midPoint = hullStack.Pop();
                PointF tailPoint = hullStack.Peek();
                PointF headPoint = pointList[i];
                hullStack.Push(midPoint); //Push midpoint back onto the stack
                Vector headVector = new Vector(headPoint.X - midPoint.X, headPoint.Y - midPoint.Y);
                Vector tailVector = new Vector(midPoint.X - tailPoint.X, midPoint.Y - tailPoint.Y);
                while (tailVector.crossProduct(headVector).deltaZ <= 0) //Backtrack & include co-linears
                {
                    if (hullStack.Count > 2)
                    {
                        hullStack.Pop();
                        midPoint = hullStack.Pop();
                        tailPoint = hullStack.Peek();
                        hullStack.Push(midPoint);
                        headVector = new Vector(headPoint.X - midPoint.X, headPoint.Y - midPoint.Y);
                        tailVector = new Vector(midPoint.X - tailPoint.X, midPoint.Y - tailPoint.Y);
                        continue;
                    }
                    else if (i == pointList.Count - 1)
                        break;
                    else
                    {
                        //our 2nd hull point really isn't a hull point.
                        hullStack.Pop();
                        i++;
                        break;
                    }
                }
                hullStack.Push(headPoint);
            }
            hullStack.Push(pointList[0]); //Connect the endpoint with the start point
            return hullStack.ToList();
        }
        //private static List<PointF> CyclicHull(List<PointF> pointList)
        //{
        //    List<PointF> cycleList = new List<PointF>();
        //    cycleList.Capacity = pointList.Count;

        //    int totalPoints = pointList.Count;
        //    while (pointList.Count > 0)
        //    {
        //        cycleList.AddRange(ConvexHull(pointList, true));
        //        pointList = pointList.Except(cycleList).ToList();
        //    }

        //    return cycleList;
        //}
        private static Graph CyclicHull(List<PointF> pointList)
        {
            List<Node> nodeList = new List<Node>();
            while (pointList.Count != 0)
            {
                var outerHull = ConvexHull(pointList, true);
                {// Link hull points together & add to nodeList
                    if (outerHull.Count > 1)
                        outerHull.RemoveAt(outerHull.Count - 1); //the first & last pt are the same, drop one of them.
                    List<Node> tempList = new List<Node>();
                    foreach (var pt in outerHull)
                    {
                        tempList.Add(new Node(pt));
                    }
                    for (int i = 1; i < tempList.Count; i++)
                    {
                        Node.Link(tempList[i], tempList[i - 1]);
                    }
                    Node.Link(tempList[0], tempList[tempList.Count - 1]); //link the start node with the end node
                    nodeList.AddRange(tempList);
                }
                pointList = pointList.Where(p => !outerHull.Any(h => h.X == p.X && h.Y == p.Y)).ToList(); //remove outerhull points
            }
            return new Graph(nodeList);

        }

        private static List<PointF> Uncross(List<PointF> pointList)
        {
            //Begin Uncrossing (Checks single intersections, Fails on 2 or more simultaneous intersections)
            while (pointList.hasIntersection()) // Continue working until all intersections are resolved
            {
                for (int i = 2; i < pointList.Count - 1; i++)
                {
                    Vector v1 = new Vector(pointList[i].X, pointList[i].Y, pointList[i + 1].X, pointList[i + 1].Y);
                    for (int j = 0; j < i - 1; j++)
                    {

                        Vector v2 = new Vector(pointList[j].X, pointList[j].Y, pointList[j + 1].X, pointList[j + 1].Y);
                        if (v1.isIntersecting2D(v2))
                        {
                            pointList.Reverse(j + 1, i - (j - 1)); // reverse points from j+1 to i (inclusive)
                            continue;
                        }
                    }
                }
            }
            return pointList;
        }
    }//End Class
}
