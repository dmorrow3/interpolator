﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using InterpolationTester.Math;
using InterpolationTester.Containers;

namespace InterpolationTester.Algorithms
{
    static class Extensions
    {
        public static bool hasIntersection(this List<PointF> list)
        {
            for (int i = 2; i < list.Count - 1; i++)
            {
                Vector v1 = new Vector(list[i].X, list[i].Y, list[i + 1].X, list[i + 1].Y);
                for (int j = 0; j < i - 1; j++)
                {
                    Vector v2 = new Vector(list[j].X, list[j].Y, list[j + 1].X, list[j + 1].Y);
                    if (v1.isIntersecting2D(v2))
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        public static float getDistance(this List<PointF> list)
        {
            float distance = 0;
            for (var i = 0; i < list.Count - 1; i++)
            {
                Vector v = new Vector(list[i + 1].X - list[i].X, list[i + 1].Y - list[i].Y);
                distance += v.getMagnitude();
            }
            return distance;
        }

        public static int indexOfminXPoint(this List<PointF> list)
        {
            int index = 0;
            float min = float.MaxValue;
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].X < min)
                {
                    index = i;
                    min = list[i].X;
                }
            }
            return index;
        }
        public static int indexOf<T>(this IEnumerable<T> list, Func<T, bool> predicate)
        {
            int index = 0;
            foreach (var item in list)
            {
                if (predicate(item))
                    return index;
                else
                    index++;
            }
            return -1;
        }

        public static List<Node> DeepCopy(this List<Node> list)
        {
            List<Node> copyList = new List<Node>();
            foreach (var node in list)
            {
                copyList.Add(Node.Copy(node));
            }
            return copyList;
        }
    }
}
